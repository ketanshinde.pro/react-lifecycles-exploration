import React, { Component } from 'react'
import logo from './logo.svg';
import './App.css';

import Lifecycles from './components/lifecycles.components'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showChild: true,
      text: ""
    }
  }

  render() {
    const { showChild, text } = this.state

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <button
            onClick={() =>
              this.setState((state) => ({
                showChild: !state.showChild
              })
              )}
          >
            Toggle Lifcycles
          </button>
          <button
            onClick={() =>
              this.setState((state) => ({
                text: state.text + "_Hello"
              })
              )}
          >
            Update Text
          </button>
          {showChild ? <Lifecycles text={text} /> : null}
        </header>
      </div>
    )
  }
}


import React, { Component } from 'react'

export default class Lifecycles extends Component {
    constructor(props) {
        super(props)

        console.log("Lifecycles Constructor!")
    }

    componentDidMount(){
        console.log("Lifecycles componentDidMount!")
    }

    componentDidUpdate(){
        console.log("Lifecycles componentDidUpdate!")
    }

    componentWillUnmount(){
        console.log("Lifecycles componentWillUnmount!")
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log("Lifecycles shouldComponentUpdate!")
        return nextProps.text !== this.props.text
    }
    
    render() {
        const { text } = this.props

        return (
            <div>
                <h3>Lifecycles Component</h3>
                {text}
            </div>
        )
    }
}
